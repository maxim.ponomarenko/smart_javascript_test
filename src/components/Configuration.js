import React from 'react';

export default function Configuration() {
    return (<div className="configurationBox">
        <h2>Configuration</h2>
        <section>
            <h3>Questions types in test</h3>

            <label htmlFor="poll"> Poll </label>
            <input type="checkbox" name="poll" id="poll"/>

            <label htmlFor="multiple"> Multiple </label>
            <input type="checkbox" name="poll" id="multiple"/>

            <label htmlFor="write"> Write </label>
            <input type="checkbox" name="poll" id="write" />
        </section>
        <section>
            <h3>Number Settings</h3>
            <label htmlFor="numberOfQuestions">Number of questions</label>
            <input type="number" name="" id="numberOfQuestions" max="100" min="1" value="5"/>
        </section>
        <section>
            <h3>Time settings</h3>
            <select name="" id="">
                <option value="">No Time</option>
                <option value="">Time Per Question</option>
                <option value="">Time Per Test</option>
            </select>
            <input type="number" name="Minutes" id="timeMinutes" title="Set Minutes"/>
            <input type="number" name="Seconds" id="timeSeconds" title="Set Seconds"/>
        </section>

    </div>
    );
}
