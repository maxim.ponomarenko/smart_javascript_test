"use strict"
/*This file will represent the server work*/

function generateTest(userTestConfig){
    let testArr = [];

    /*There will be generating logic*/
    testArr[0] = questionsArrPoll[0];
    testArr[0].type = 'poll';

    testArr[1] = questionsArrCheckbox[0];
    testArr[1].type = 'checkbox';

    testArr[2] = questionsArrWrite[0];
    testArr[2].type = 'write';
    /*Above will be generation logic*/

    return testArr;
}