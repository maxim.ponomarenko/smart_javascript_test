"use strict"
/*This file will represent database with questions */

//class Question unites common properties for all question types (poll, checkbox, write)
class Question{
    constructor(Id, QuestionText, TagsArr, AnswerExplanation){
        this.id = Id;//Task id to give user a opportunity of bugfixes and asks
        this.questionText = QuestionText;//Multiline string with question`s text

        this.tagsArr = TagsArr;
        //Unreleased
        this.answerExplanation = AnswerExplanation;
        //All questions will have some explanation text after finishing the test
    }
}

class PollQuestion extends Question{
    constructor(
        Id, QuestionText, AnswersArr, RightAnswer, TagsArr, AnswerExplanation
    ){
        super(Id, QuestionText, TagsArr, AnswerExplanation);
        this.answersArr = AnswersArr;
        this.rightAnswer = RightAnswer;

        //Setting question type of PollQuestion class
        this.type = 'poll';
    }
}

class CheckboxQuestion extends Question{
    constructor(
        Id, QuestionText, AnswersArr, RightAnswersArr, TagsArr, AnswerExplanation
    ){
        super(Id, QuestionText, TagsArr, AnswerExplanation);
        this.answersArr = AnswersArr;
        this.rightAnswersArr = RightAnswersArr;

        //Setting question type of CheckboxQuestion class
        this.type = 'checkbox';
    }
}

class WriteQuestion extends Question{
    constructor(
        Id, QuestionText, RightAnswerStr, TagsArr, AnswerExplanation
    ){
        super(Id, QuestionText, AnswerExplanation);
        this.rightAnswerStr = RightAnswerStr;//Multiline string with right answer

        //Setting question type of WriteQuestion class
        this.type = 'write';
    }
}

/*Array with type:poll questions*/
const questionsArrPoll = [
    new PollQuestion('0001',
        `What is the correct JavaScript syntax to change the content of the HTML element below?
        &lt;p id="demo"&gt;This is a demonstration.&lt;/p&gt;`,
        ['document.getElementById("demo").innerHTML="Hello World!";',
            'document.getElementByName("p").innerHTML="Hello World!";',
            '#demo.innerHTML="Hello World!";',
        'document.getElement("p").innerHTML="Hello World!";'],0, ['DOM'])]

/*Array with type:checkbox questions*/
const questionsArrCheckbox = [
    new CheckboxQuestion('0002', `dfsd?`, [2,3,4,1],[2,1],  ['arrays'])
]

/*Array with type:write questions*/
const questionsArrWrite = [
    new WriteQuestion('0003', `sdfjdsjfsdlf?`, `sdflajdfd`,  ['objects'])
]