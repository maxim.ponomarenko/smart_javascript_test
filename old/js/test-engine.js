"use strict"

//Test elements
let testArray = [];

//UI elements
const configBlock = document.getElementById("configurationBox");
const startBtn = document.getElementById("startBtn");
const answerBtn = document.getElementById("answerBtn");

//Question elements
let questionText = document.getElementById("questionText");
const pollAnswers = document.getElementById("pollAnswers");
const checkboxAnswers = document.getElementById("checkboxAnswers");
const writeAnswer = document.getElementById("writeAnswer");



//Function
let displayQuestion = function (questionObject){
    startBtn.style.display = 'none';//hiding startBtn, when quiz starts
    questionText.style.display = 'block';//displaying question box
    questionText.innerHTML = questionObject.questionText;

    if(questionObject.type === 'poll'){
        pollAnswers.style.display = 'block';

        for(let i=0; i<questionObject.answersArr.length; i++){
            let radioAnswerBlock = document.createElement('div');
            radioAnswerBlock.classList.add('radioAnswer');

            let radioInput = document.createElement('input');
            radioInput.setAttribute('type', 'radio');
            radioInput.setAttribute('id', 'radioInput'+i);
            radioInput.setAttribute('name', questionObject.id);

            let radioLabel = document.createElement('label');
            radioLabel.setAttribute('for', 'radioInput'+i);
            radioLabel.innerHTML = questionObject.answersArr[i] + '\n';

            radioAnswerBlock.append(radioInput);
            radioAnswerBlock.append(radioLabel);

            pollAnswers.append(radioAnswerBlock);
        }
    }
    else if(questionObject.type === 'checkbox'){

    }
    else if(questionObject.type === 'write'){

    }
    else {
        //error realisation
        console.log('incorrect type');
    }

    answerBtn.style.display = "block";
}

//Function
function startTest(){
    //Hiding configuration box
    configBlock.style.display = "none";

    testArray = generateTest('there will be config');
    displayQuestion(testArray[0]);
}




startBtn.addEventListener('click', function(){ startTest() })


